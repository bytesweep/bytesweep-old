#!/bin/bash

DBPASS="dbpass"
sudo docker build -t bytesweep-db -f bytesweep-db/Dockerfile .
sudo docker build -t bytesweep-web --build-arg DBPASS="$DBPASS" -f bytesweep-web/Dockerfile .
sudo docker build -t bytesweep-worker --build-arg DBPASS="$DBPASS" -f bytesweep-worker/Dockerfile .
sudo docker build -t bytesweep-cvefetch --build-arg DBPASS="$DBPASS" -f bytesweep-cvefetch/Dockerfile .
sudo docker build -t bytesweep-watchdog --build-arg DBPASS="$DBPASS" -f bytesweep-watchdog/Dockerfile .
