# Bytesweep Docker Containers

## Dev Environment Setup

NOTE: if you have something else listening on ports 8000 or 5432 this will fail.

1. clone this repo
```
git clone https://gitlab.com/bytesweep/bytesweep.git
cd bytesweep
```

2. make a directory called 'repos' and clone the 4 ByteSweep code repos to it
```
mkdir repos
cd repos
git clone https://gitlab.com/bytesweep/bytesweep-web.git
git clone https://gitlab.com/bytesweep/bytesweep-worker.git
git clone https://gitlab.com/bytesweep/bytesweep-cvefetch.git
git clone https://gitlab.com/bytesweep/bytesweep-watchdog.git
```

3. Build docker containers. This may take awhile.
```
./build-bytesweep.sh
```

4. Start docker containers
```
./start-bytesweep.sh
```

5. Navigate to bytesweep at http://127.0.0.1:8000

- username: admin
- password: pass

