ByteSweep

# Intro

ByteSweep is a Free Software IoT security analysis platform. This platform will allow IoT device makers, large and small, to conduct fully automated security checks before they ship firmware.

ByteSweep Features:
- Firmware extraction
- File data enrichment
- Key and password hash identification
- Unsafe function use detection
- 3rd party component identification
- CVE correlation

# Docker Dev Environment

Check out [dev-environment/README.md](./dev-environment/README.md) to get up and running quickly.

# Install

Check out [INSTALL.md](./INSTALL.md) for more stable deployment options.


# Default Login

- username: admin
- password: pass
